import asyncio
import functools
import http
import json
import os, sys
import subprocess

import websockets


def check(rm_hostname):
    try:
        model = subprocess.run(
            [
                "ssh",
                "-o",
                "ConnectTimeout=2",
                rm_hostname,
                "cat",
                "/proc/device-tree/model",
            ],
            check=True,
            capture_output=True,
        )
        return model.stdout[:14].decode("utf-8")
    except subprocess.CalledProcessError:
        print(f"Error: Can't connect to reMarkable tablet on hostname : {rm_hostname}")
        os._exit(1)

# From linux/input-event-codes.h
# Event types:
EV_SYN = 0x00
EV_KEY = 0x01
EV_REL = 0x02
EV_ABS = 0x03
BTN_TOOL_PEN = 0x140
BTN_TOOL_RUBBER = 0x141

async def websocket_handler(websocket, rm_host, rm_model):
    if rm_model == "reMarkable 1.0":
        device = "/dev/input/event0"
    elif rm_model == "reMarkable 2.0":
        device = "/dev/input/event1"
    else:
        raise NotImplementedError(f"Unsupported reMarkable Device : {rm_model}")

    # The async subprocess library only accepts a string command, not a list.
    command = f"ssh -o ConnectTimeout=2 {rm_host} cat {device}"

    x = 0
    y = 0
    pressure = 0

    proc = await asyncio.create_subprocess_shell(
        command, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )
    print("Started process")

    try:
        # Keep looping as long as the process is alive.
        # Terminated websocket connection is handled with a throw.
        while proc.returncode == None:
            buf = await proc.stdout.read(16)

            # TODO expect 16-bit chunks, or no data.
            # There are synchronisation signals in the data stream, maybe use those
            # if we drift somehow.
            if len(buf) >= 16:
                timestamp = f'{int.from_bytes(buf[0:4],"little")}.' + \
                            f'{int.from_bytes(buf[4:8],"little")}'
                # Using notes from https://github.com/ichaozi/RemarkableFramebuffer
                # or https://github.com/canselcik/libremarkable/wiki
                typ  = int.from_bytes(buf[8:10], 'little')
                code = int.from_bytes(buf[10:12], 'little')
                val  = int.from_bytes(buf[12:16], 'little')

                # Absolute position.
                if typ == EV_ABS:
                    if code == 0:
                        x = val
                    elif code == 1:
                        y = val
                    elif code == 24:
                        pressure = val
                    await websocket.send(json.dumps((x, y, pressure)))
                elif typ == EV_KEY: # key, e.g. tool change
                    if code == BTN_TOOL_PEN or code == BTN_TOOL_RUBBER:
                        await websocket.send(json.dumps([code]))
                else:
                    #print(f'timestamp: {str(timestamp):29} type={typ} code={code} {buf[14:]}',
                    #        file = sys.stderr)
                    pass

        print("Disconnected from ReMarkable.")

    finally:
        print("Disconnected from browser.")
        proc.kill()


async def http_handler(path, request):
    # only serve index file or defer to websocket handler.
    if path == "/websocket":
        return None

    elif path != "/":
        return (http.HTTPStatus.NOT_FOUND, [], "")

    body = open("index.html", "rb").read()
    headers = [
        ("Content-Type", "text/html"),
        ("Content-Length", str(len(body))),
        ("Connection", "close"),
    ]

    return (http.HTTPStatus.OK, headers, body)


def run(rm_host="root@10.11.99.1", host="localhost", port=6789):
    rm_model = check(rm_host)
    bound_handler = functools.partial(
        websocket_handler, rm_host=rm_host, rm_model=rm_model
    )
    start_server = websockets.serve(
        bound_handler, host, port, ping_interval=1000, process_request=http_handler
    )

    print(f"Visit http://{host}:{port}/")

    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()


if __name__ == "__main__":
    run()
